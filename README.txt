For Mac Users, install as instruction listed below:
1. Store the orangexchange folder in any directory, such as Desktop, or Dropbox
2. Install pip:
	- make sure install pip in the same directory as orangexchange is stored.
	- sudo easy_install pip
3. Install virtualenv:
	- pip install virtualenv

4. Activate virtualenv:
	- cd orangexchange
	- source bin/activate

5. Install required apps
	- pip install -r requirements.txt

6. Runserver
	- cd src
	- python manage.py runserver

7. Server Address: http://127.0.0.1:8000

8. Administrator Login @ http://127.0.0.1:8000/admin
	- username: orangexchange
	- password: 123456

