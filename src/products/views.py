from django.shortcuts import render, RequestContext, render_to_response, Http404, HttpResponseRedirect
from django.template.defaultfilters import slugify
from .models import Product, Category
from django.contrib.auth.decorators import login_required
from .forms import ProductForm

# Create your views here.

"""This function is to serve the detail info of each product in database
"""
@login_required
def product_detail(request, cat_slug, product_slug):
	try:
		cat = Category.objects.get(slug=cat_slug)
	except:
		raise Http404
	try:
		obj = Product.objects.get(slug=product_slug)
		return render(request, "products/product_detail.html", {"obj": obj})
	except: 
		raise Http404

"""List of products
"""
def product_list(request):
	queryset = Product.objects.all()
	context ={
		"queryset": queryset
	}
	return render(request, "products/product_list.html", context)


"""List of category
"""
def category_list(request):
	queryset = Category.objects.all()
	context ={
		"queryset": queryset
	}
	return render(request, "products/category_list.html", context)

"""Category detail
"""
@login_required
def category_detail(request, cat_slug):
	try:
		obj = Category.objects.get(slug=cat_slug)
		queryset = obj.product_set.all()
		return render(request, "products/product_list.html", {"obj": obj, "queryset": queryset})
	except: 
		raise Http404

"""add function allows users to upload books/items to the database
   Users are required to login
"""
@login_required
def add(request):
	form = ProductForm(request.POST or None)
	if form.is_valid():
		product_upload = form.save(commit=False)
		product_upload.user = request.user
		product_upload.slug = slugify(form.cleaned_data['title'])
		product_upload.save()
		confirm_message = """
		Book Added!
		"""
		return HttpResponseRedirect("/")

	return render_to_response("products/add.html", locals(), context_instance=RequestContext(request))


