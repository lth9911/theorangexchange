from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
# Create your models here.

class ProductQueryset(models.query.QuerySet):
	def active(self):
		return self.filter(active=True)

class ProductManager(models.Manager):
	def get_queryset(self):
		return ProductQueryset(self.model, using=self._db)

	def get_featured(self):
		return self.get_queryset().active().featured()
		# return super(ProductManager, self).filter(featured=True)
	def all(self):
		return self.get_queryset().active()

"""This class is to save information that are related to each Product

"""
class Product(models.Model):
	user = models.ForeignKey(User, null=True, blank=True)
	title = models.CharField(max_length=180)
	author = models.CharField(max_length=180, null=True, blank=True)
	isbn = models.CharField(default="ISBN number", max_length=10)
	condition = models.CharField(max_length=500)
	course = models.CharField(max_length=180, null=True, blank=True)
	image = models.ImageField(upload_to='images/', null=True, blank=True)
	price = models.DecimalField(max_digits=20, decimal_places=2)
	slug = models.SlugField(null=True, blank=True)
	category = models.ForeignKey("Category", default=1)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
	updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)
	active = models.BooleanField(default=True)
	featured = models.BooleanField(default=False)

	objects=ProductManager()
	
	class Meta:
		unique_together = ('slug', 'category')
	def __unicode__(self):
		return str(self.title)

	def get_absolute_url(self):
		return reverse("product_detail", kwargs={"product_slug":self.slug, "cat_slug":self.category.slug})

	def get_image_url(self):
		return "%s%s" %(settings.MEDIA_URL, self.image)


class Tag(models.Model):
	product = models.ForeignKey(Product)
	tag = models.CharField(max_length=20)
	slug = models.SlugField()
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __unicode__(self):
		return str(self.tag)


"""Each product/item belongs to 1 Category, but 1 Category can contains alot of items
   Category model saves the infos related to each category
"""

class Category(models.Model):
	#products = models.ManyToManyField(Product, null=True, blank=True)
	title = models.CharField(max_length=120)
	description = models.TextField(max_length=5000, null=True, blank=True)
	#image = models.ImageField(upload_to='images/', null=True, blank=True)
	slug = models.SlugField(default='MajorCourse', unique='True')
	active = models.BooleanField(default=True)
	featured = models.BooleanField(default=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __unicode__(self):
		return str(self.title)

	class Meta:
		verbose_name = "Category"
		verbose_name_plural = "Categories"

	def get_absolute_url(self):
		return reverse("product_detail", kwargs={"cat_slug": self.category.slug})


