# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20150213_2328'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='categoryimage',
            name='category',
        ),
        migrations.DeleteModel(
            name='CategoryImage',
        ),
        migrations.RemoveField(
            model_name='productimage',
            name='product',
        ),
        migrations.DeleteModel(
            name='ProductImage',
        ),
    ]
