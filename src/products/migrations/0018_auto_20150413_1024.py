# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0017_auto_20150413_1020'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='coursecode',
            new_name='course',
        ),
    ]
