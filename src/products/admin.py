from django.contrib import admin
from .models import Product, Category, Tag


# Register your models here.

class TagInline(admin.TabularInline):
 	model = Tag



class ProductAdmin(admin.ModelAdmin):
	list_display = ["__unicode__", 'slug']
	fields = ['title', 'author', 'isbn', 'course', 'price', 'condition', 'image', 'active', 'featured', 'category']
	list_filter = ['price']
	class Meta:
		model = Product


admin.site.register(Product, ProductAdmin)


class CategoryAdmin(admin.ModelAdmin):
	class Meta:
		model = Category

admin.site.register(Category, CategoryAdmin)
