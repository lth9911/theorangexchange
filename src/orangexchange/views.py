from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from products.models import Product
from .forms import LoginForm


"""Home function handles data that will be showed on homepage
   Users are required to register/ login
"""
@login_required
def home(request):
	template = 'home.html'
	products = Product.objects.filter(active=True)
	context = {		
		"number": products.count(),
		"products": products,
	}	
	

	return render(request, template, context)



"""These functions handle the pages that are responding to each function's name
"""
def about(request):
	context = locals()
	template = 'about.html'

	return render(request, template, context)

def faq(request):
	context = locals()
	template = 'faq.html'

	return render(request, template, context)

def terms(request):
	context = locals()
	template = 'terms.html'

	return render(request, template, context)



"""Login/Logout functions
"""
def auth_logout(request):
	logout(request)
	return HttpResponseRedirect('/')


def auth_login(request):
	form = LoginForm(request.POST or None)
	next_url = request.GET.get('next')
	if form.is_valid():
		username = form.cleaned_data['username']
		password = form.cleaned_data['password']

		#print authenticate(username=username, password=password)
		
		#check authenticated username, pwd
		user = authenticate(username=username, password=password)
		if user is not None:
			login(request, user)
			return HttpResponseRedirect(next_url)
	context={"form": form}

	return render(request, "login.html", context)