from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = patterns('',    
    (r'^accounts/', include('allauth.urls')),
    url(r'^$', 'orangexchange.views.home', name='home'),
    url(r'^contact/$', 'contact.views.home', name='contact'),
    url(r'^myexchange/$', 'profiles.views.user_profile', name='profile'),

    #info. url
    url(r'^about/$', 'orangexchange.views.about', name='about'),
    url(r'^terms/$', 'orangexchange.views.terms', name='terms'),
    url(r'^faq/$', 'orangexchange.views.faq', name='faq'),


    #product url
    url(r'^products/$', 'products.views.category_list', name='category_list'),
    url(r'^products/(?P<slug>[\w-]+)/$', 'products.views.category_detail', name='category_detail'), 
    url(r'^products/(?P<cat_slug>[\w-]+)/(?P<product_slug>[\w-]+)/$', 'products.views.product_detail', name='product_detail'),    
    url(r'^add/$', 'products.views.add', name='add_product'),


    # url(r'^products/$', 'products.views.product_list', name='product_list'),
    # url(r'^products/(?P<id>\d+)/$', 'products.views.product_detail', name='product_detail'),
    # url(r'^all/$', 'products.views.list_all', name="all_products"),

    #myexchange url
    url(r'^myexchange/$', 'products.views.category_list', name='category_list'),

    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


# auth login/out
urlpatterns += patterns('orangexchange.views', 
    url(r'^login/$', 'auth_login', name='login'),
    url(r'^logout/$', 'auth_logout', name='logout'),
)

# serving media files
if settings.DEBUG:
    urlpatterns += patterns('',) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

