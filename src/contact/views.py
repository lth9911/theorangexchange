from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render

from .forms import ContactForm
# Create your views here.



"""Allow guests to send message to the administrator
"""
def home(request):
	title = "Contact Us"
	form = ContactForm(request.POST or None)
	confirm_message = None
	#getting msg from users on Contact page
	if form.is_valid():
		comment = form.cleaned_data['comment']
		name = form.cleaned_data['name']
		sbj = 'Message from OrangeXchange.com'
		msg = '%s %s' %(comment, name)
		frm = form.cleaned_data['email']
		to_us = [settings.EMAIL_HOST_USER]
		print sbj, msg, frm, to_us

		#store the message
		send_mail(sbj, msg, frm, to_us, fail_silently=True)
		title = "Thank You"
		confirm_message = """
		Thank you for your message!
		"""
		form = None
	context = {
		'title': title,
		'form': form,
		'confirm_message': confirm_message

	}

	
	template = 'contact.html'

	return render(request, template, context)


