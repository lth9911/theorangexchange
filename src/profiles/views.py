from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from products import models, forms

# Create your views here.
def home(request):
	context = locals()
	template = 'home.html'

	return render(request, template, context)


@login_required
def user_profile(request):
	user = request.user
	context = {'user': user}
	template = 'myexchange.html'

	return render(request, template, context)
