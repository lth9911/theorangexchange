Django==1.7.3
django-allauth==0.19.0
django-crispy-forms==1.4.0
oauthlib==0.7.2
Pillow==2.7.0
python-openid==2.2.5
requests==2.5.1
requests-oauthlib==0.4.2
